package com.udemy.weather.openweathermap

// https://www.udemy.com/course/android-kotlin-apps-mobiles
data class Weather(val description: String, val temperature: Float, val humidity: Int, val pressure: Int, val iconUrl: String)