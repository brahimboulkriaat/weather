package com.udemy.weather.openweathermap

import com.google.gson.annotations.SerializedName

class WeatherWrapper(val weather: Array<WeatherData>, val main: MainData)

class WeatherData(val description: String, val icon: String)

class MainData(@SerializedName("temp") val temperature: Float, val pressure: Int, val humidity: Int)

// https://www.udemy.com/course/android-kotlin-apps-mobiles