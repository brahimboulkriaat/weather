package com.udemy.weather.openweathermap

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

private const val API_KEY = "343adb07ba22708eeb36a0d0cb19fe5c"

interface OpenWeatherService {

    @GET("data/2.5/weather?units=metric&lang=fr")
    fun getWeather(@Query("q") cityName: String, @Query("appid") apiKey: String = API_KEY) : Call<WeatherWrapper>
}

// https://www.udemy.com/course/android-kotlin-apps-mobiles