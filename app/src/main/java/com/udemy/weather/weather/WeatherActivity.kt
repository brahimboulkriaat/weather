package com.udemy.weather.weather

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.udemy.weather.R

// https://www.udemy.com/course/android-kotlin-apps-mobiles
class WeatherActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportFragmentManager
                .beginTransaction()
                .replace(android.R.id.content, WeatherFragment.newInstance())
                .commit()
    }
}